<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Dashboard</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <link rel="stylesheet" href="style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
</head>
<body>
<div class="wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header clearfix">
                    <h2 class="pull-left">Tables</h2>
                    <a href="view.php" class="btn btn-success pull-right">View elements</a>
                </div>
                <?php
                // Include config file
                require_once "database.php";

                $lenght;
                $prenoms = array();
                // Attempt select query execution
                $sql = "SELECT * FROM users";
                $result = $pdo->query($sql);
                    while($row=$result->fetch()){
                        array_push($prenoms, $row['name']);
                        $lenght=sizeof($prenoms)-1;
                    }
                    for ($i=0; $i<4; $i++){
                        $a = $i+1;
                        echo "<div style='background-color: darksalmon; width: 400px; font-weight: bold'> Table n° : $a</div> ";
                        for ($j=0; $j<4; $j++){
                        $aleatoire = rand(0 , $lenght);
                        echo "<table style='background-color: aqua; width: 100px; border: solid 1px gray; float: left;padding: 10px'>";
                        echo "<td>$prenoms[$aleatoire]</td>";
                        echo "</table>";

                        unset($prenoms[$aleatoire]);
                        $prenoms = array_values($prenoms);
                        $lenght--;
                    }

                    echo "<br />";
            }
                        // Free result set
                        unset($result);

                // Close connection
                unset($pdo);
                ?>
            </div>
        </div>

    </div>
</div>

</body>
</html>