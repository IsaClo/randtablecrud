CREATE TABLE users (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(20) NOT NULL
);

INSERT INTO `users`(`name`) VALUES ('Kiki'), ('Laurie'),  ('Syriane'), ('Nathalie') ,('Flo'), ('Maude'),  ('Martin'), ('Otilia'), ('Léa'), ('Viviane'),  ('Ester'), ('Sixtine'), ('Esma'), ('Hayette'),  ('Grace'), ('Isa');