<?php require 'database.php';

$nameError = null;
$name = htmlentities(trim($_POST['name']));
$valid = true;
if (empty($name)) {
    $nameError = 'Please enter Name';
    $valid = false;
}

if($_SERVER["REQUEST_METHOD"]== "POST" && !empty($_POST)){ //on initialise nos messages d'erreurs; $nameError = ''; $firstnameError=''; $ageError=''; $telError =''; $emailError =''; $paysError=''; $commentError=''; $metierError=''; $urlError=''; // on recupère nos valeurs $name = htmlentities(trim($_POST['name'])); $firstname=htmlentities(trim($_POST['firstname'])); $age = htmlentities(trim($_POST['age'])); $tel=htmlentities(trim($_POST['tel'])); $email = htmlentities(trim($_POST['email'])); $pays=htmlentities(trim($_POST['pays'])); $comment=htmlentities(trim($_POST['comment'])); $metier=htmlentities(trim($_POST['metier'])); $url=htmlentities(trim($_POST['url'])); // on vérifie nos champs $valid = true; if (empty($name)) { $nameError = 'Please enter Name'; $valid = false; }else if (!preg_match("/^[a-zA-Z ]*$/",$name)) { $nameError = "Only letters and white space allowed"; } if(empty($firstname)){ $firstnameError ='Please enter firstname'; $valid= false; }else if (!preg_match("/^[a-zA-Z ]*$/",$name)) { $nameError = "Only letters and white space allowed"; } if (empty($email)) { $emailError = 'Please enter Email Address'; $valid = false; } else if ( !filter_var($email,FILTER_VALIDATE_EMAIL) ) { $emailError = 'Please enter a valid Email Address'; $valid = false; } if (empty($age)) { $ageError = 'Please enter your age'; $valid = false; } if (empty($tel)) { $telError = 'Please enter phone'; $valid = false; }else if(!preg_match("#^0[1-68]([-. ]?[0-9]{2}){4}$#",$tel)){ $telError = 'Please enter a valid phone'; $valid = false; } if (!isset($pays)) { $paysError = 'Please select a country'; $valid = false; } if(empty($comment)){ $commentError ='Please enter a description'; $valid= false; } if(empty($metier)){ $metierError ='Please select a job'; $valid= false; } if(empty($url)){ $urlError ='Please enter website url'; $valid= false; } else if(!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$url)) { $urlError='Enter a valid url'; $valid=false; } // si les données sont présentes et bonnes, on se connecte à la base if ($valid) { $pdo = Database::connect(); $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "INSERT INTO `users`(`name`) VALUES (?)";
            $q = $pdo->prepare($sql);
            $q->execute(array($name));
            header("Location: index.php");

    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Crud</title>

    </head>
    <body>

        <div class="container">

                <div class="row">

                <h3>Ajouter un contact</h3>

                </div>


            <form method="post" action="create.php">

                <div class="control-group <?php echo !empty($nameError)?'error':'';?>">
                    <label class="control-label">Name</label>

                    <div class="controls">
                        <input name="name" aria-label="" type="text"  placeholder="Name" value="<?php echo !empty($name)?$name:'';?>">
                        <?php if (!empty($nameError)): ?>
                        <span class="help-inline"><?php echo $nameError;?></span>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="form-actions">
                                 <input type="submit" class="btn btn-success" name="submit" value="submit">
                                 <a class="btn" href="index.php">Retour</a>
                </div>

            </form>
        </div>

    </body>
</html>